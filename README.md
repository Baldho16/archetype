# Subscriptions Platform Adapter Archetype

Archetype used to generate a Dropwizard application that implements a subscriptions adapter for integrating a new aggregator, master aggregator or carrier.
<br/>
## Prerequisites
- JDK 8
- Maven 3.3.9 or higher.

## Build
```bash
mvn clean install -U -DskipTests -P ${MAVEN_PROFILE}
```
<br/>
Available profiles:
  * `releases`: For releasing final versions.
  * `snapshots`: For releasing snapshot versions.

## How to use this archetype

Execute the following command to create a new maven project that implements a subscriptions adapter:

```bash
mvn archetype:generate -DarchetypeGroupId=com.amk.sva.subs.it -DarchetypeArtifactId=subs-platform-adapter-archetype -DarchetypeVersion=${ARCHETYPE_VERSION} \ 
-DgroupId=com.amk.sva.subs.it -DartifactId=${SUBS_ADAPTER_ARTIFACT_ID} -Dversion=${SUBS_ADAPTER_VERSION} -Dpackage=${SUBS_ADAPTER_PACKAGE} \ 
-DappName=${DROPWIZARD_APP_NAME_PREFIX} -DprojectName=${MAVEN_PROJECT_NAME} -DprojectDescription=${MAVEN_PROJECT_DESCRIPTION} \ 
-DgitRepository=${GIT_REPOSITORY_URL} -DsvaBomVersion=${SVA_BOM_VERSION}
```

<br/>
Where:
  * `${ARCHETYPE_VERSION}`: The version of Subscriptions Platform Adapter Archetype to use. Example: `1.0.0`
  * `${SUBS_ADAPTER_ARTIFACT_ID}`: Artifact id of the maven project that will implement the new subscriptions adapter. Example: `subs-fortumo-adapter`
  * `${SUBS_ADAPTER_VERSION}`: Version of the maven project that will implement the new subscriptions adapter. Default: `1.0.0-SNAPSHOT`
  * `${SUBS_ADAPTER_PACKAGE}`: Base package of the source code. Example: `com.amk.sva.subs.it.fortumo`
  * `${DROPWIZARD_APP_NAME_PREFIX}`: Prefix for naming the application and configuration classes of the Dropwizard microservice. Example: `SubscriptionsFortumoAdapter`
  * `${MAVEN_PROJECT_NAME}`: Project name. This variable will be set in the `project.name` property of the pom.xml file. Example: `"Subscriptions Fortumo Adapter"`
  * `${MAVEN_PROJECT_DESCRIPTION}`: Project description that will be set in the the pom.xml file. Example: `"This project implements a microservice to integrate the Subscriptions system with the Fortumo platform to generate new subscriptions."`
  * `${GIT_REPOSITORY_URL}`: The URL of the git repository of the project.
  * `${SVA_BOM_VERSION}`: The version of the SVA BOM to use in the new maven project. Default: `1.0.2`

<br/>
Example:
```bash
mvn archetype:generate -DarchetypeGroupId=com.amk.sva.subs.it -DarchetypeArtifactId=subs-platform-adapter-archetype -DarchetypeVersion=1.0.0 \ 
-DgroupId=com.amk.sva.subs.it -DartifactId=subs-fortumo-adapter -Dversion=1.0.0-SNAPSHOT -Dpackage=com.amk.sva.subs.it.fortumo \ 
-DappName=SubscriptionsFortumoAdapter -DprojectName="Subscriptions Fortumo Adapter" -DprojectDescription="This project implements a microservice to integrate the Subscriptions system with the Fortumo platform to generate new subscriptions." \ 
-DgitRepository=ssh://git@ic.amick.mx:1022/sva-subscriptions/integrations/fortumo/subs-fortumo-adapter.git -DsvaBomVersion=1.0.2
```
