#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $str = "MMM dd, yyyy" )
#set( $ldt = $package.getClass().forName("java.time.LocalDateTime").getMethod("now").invoke(null) )
#set( $dtf = $package.getClass().forName("java.time.format.DateTimeFormatter").getMethod("ofPattern", $package.getClass().forName("java.lang.String")).invoke(null, $str) )
#set( $dtfYear = $package.getClass().forName("java.time.format.DateTimeFormatter").getMethod("ofPattern", $package.getClass().forName("java.lang.String")).invoke(null, "yyyy") )
#set( $date = $ldt.format($dtf) )
#set( $year = $ldt.format($dtfYear) )
#set( $cleanVersion = "${version}" )
#set( $cleanVersion = $cleanVersion.replaceAll("-SNAPSHOT", "") )
/*
 *      File: CORSResponseFilter.java
 *    Author: Horacio Ferro <horacio.ferro@amk-technologies.com>
 *      Date: ${date}
 * Copyright: AMK Technologies, S.A. de C.V. ${year}
 */
package ${package}.filters;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;

/**
 * Filter used to add the CORS headers to the responses.
 *
 * @author Horacio Ferro &lt;horacio.ferro@amk-technologies.com&gt;
 * @version ${cleanVersion}
 * @since ${cleanVersion}
 */
public class CORSResponseFilter implements ContainerResponseFilter {

    /**
     * Adds the CORS filters to the response.
     *
     * @param requestContext The context of the HTTP request.
     * @param responseContext The context of the HTTP response.
     * @see javax.ws.rs.container.ContainerResponseFilter${symbol_pound}filter(javax.ws.rs.container.ContainerRequestContext,
     * javax.ws.rs.container.ContainerResponseContext)
     */
    @Override
    public void filter(final ContainerRequestContext requestContext, final ContainerResponseContext responseContext)
            throws IOException {
        final MultivaluedMap<String, Object> headers = responseContext.getHeaders();
        // TODO Adjust accordingly
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        headers.add("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Codingpedia");
    }

}
