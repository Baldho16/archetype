#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $str = "MMM dd, yyyy" )
#set( $ldt = $package.getClass().forName("java.time.LocalDateTime").getMethod("now").invoke(null) )
#set( $dtf = $package.getClass().forName("java.time.format.DateTimeFormatter").getMethod("ofPattern", $package.getClass().forName("java.lang.String")).invoke(null, $str) )
#set( $dtfYear = $package.getClass().forName("java.time.format.DateTimeFormatter").getMethod("ofPattern", $package.getClass().forName("java.lang.String")).invoke(null, "yyyy") )
#set( $date = $ldt.format($dtf) )
#set( $year = $ldt.format($dtfYear) )
#set( $cleanVersion = "${version}" )
#set( $cleanVersion = $cleanVersion.replaceAll("-SNAPSHOT", "") )
/*
 *      File: package-info.java
 *    Author: Gerardo Zuniga <gerardo.zuniga@amk-technologies.com>
 *      Date: ${date}
 * Copyright: AMK Technologies, S.A. de C.V. ${year}
 */
/**
 * This package must contain all the RESTful resources exposed by the API.
 *
 * @author Gerardo Zuniga &lt;gerardo.zuniga@amk-technologies.com&gt;
 * @version ${cleanVersion}
 * @since ${cleanVersion}
 */
package ${package}.resources;