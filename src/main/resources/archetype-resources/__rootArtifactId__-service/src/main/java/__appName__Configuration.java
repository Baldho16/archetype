#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $str = "MMM dd, yyyy" )
#set( $ldt = $package.getClass().forName("java.time.LocalDateTime").getMethod("now").invoke(null) )
#set( $dtf = $package.getClass().forName("java.time.format.DateTimeFormatter").getMethod("ofPattern", $package.getClass().forName("java.lang.String")).invoke(null, $str) )
#set( $dtfYear = $package.getClass().forName("java.time.format.DateTimeFormatter").getMethod("ofPattern", $package.getClass().forName("java.lang.String")).invoke(null, "yyyy") )
#set( $date = $ldt.format($dtf) )
#set( $year = $ldt.format($dtfYear) )
#set( $cleanVersion = "${version}" )
#set( $cleanVersion = $cleanVersion.replaceAll("-SNAPSHOT", "") )
/*
 *      File: ${appName}Configuration.java
 *    Author: Gerardo Zuniga <gerardo.zuniga@amk-technologies.com>
 *      Date: ${date}
 * Copyright: AMK Technologies, S.A. de C.V. ${year}
 */
package ${package};

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.URL;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;

import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

/**
 * Configuration class of ${projectName}.
 *
 * @author Gerardo Zuniga &lt;gerardo.zuniga@amk-technologies.com&gt;
 * @version ${cleanVersion}
 * @since ${cleanVersion}
 */
public class ${appName}Configuration extends Configuration {

    /**
     * Jersey client configuration for Subscriptions Information API.
     */
    @Valid
    @NotNull
    private JerseyClientConfiguration jerseyConfigSubsInfo;
    /**
     * URL of Subscriptions Information API.
     */
    @URL
    private String urlSubsInfo;

    /**
     * Swagger configuration.
     */
    @Valid
    @NotNull
    private SwaggerBundleConfiguration swaggerConfig;

    /**
     * Gets the Jersey client configuration for Subscriptions Information API..
     *
     * @return The Jersey client configuration for Subscriptions Information API.
     */
    @JsonProperty("jerseyConfigSubsInfo")
    public JerseyClientConfiguration getJerseyConfigSubsInfo() {
        return jerseyConfigSubsInfo;
    }

    /**
     * Sets the Jersey client configuration for Subscriptions Information API.
     *
     * @param jerseyConfigSubsInfo The Jersey client configuration for Subscriptions Information API.
     */
    public void setJerseyConfigSubsInfo(final JerseyClientConfiguration jerseyConfigSubsInfo) {
        this.jerseyConfigSubsInfo = jerseyConfigSubsInfo;
    }

    /**
     * Gets the URL of Subscriptions Information API.
     *
     * @return The URL of Subscriptions Information API.
     */
    @JsonProperty("urlSubsInfo")
    public String getUrlSubsInfo() {
        return urlSubsInfo;
    }

    /**
     * Sets the URL of Subscriptions Information API.
     *
     * @param urlSubsInfo The URL of Subscriptions Information API.
     */
    public void setUrlSubsInfo(final String urlSubsInfo) {
        this.urlSubsInfo = urlSubsInfo;
    }

    /**
     * Gets the swagger configuration.
     *
     * @return The swagger configuration.
     */
    @JsonProperty("swagger")
    public SwaggerBundleConfiguration getSwaggerConfig() {
        return swaggerConfig;
    }

    /**
     * Sets the swagger configuration.
     *
     * @param swaggerConfig the swaggerConfig to set.
     */
    public void setSwaggerConfig(final SwaggerBundleConfiguration swaggerConfig) {
        this.swaggerConfig = swaggerConfig;
    }

}
