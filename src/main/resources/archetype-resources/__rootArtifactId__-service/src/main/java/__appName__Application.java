#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $str = "MMM dd, yyyy" )
#set( $ldt = $package.getClass().forName("java.time.LocalDateTime").getMethod("now").invoke(null) )
#set( $dtf = $package.getClass().forName("java.time.format.DateTimeFormatter").getMethod("ofPattern", $package.getClass().forName("java.lang.String")).invoke(null, $str) )
#set( $dtfYear = $package.getClass().forName("java.time.format.DateTimeFormatter").getMethod("ofPattern", $package.getClass().forName("java.lang.String")).invoke(null, "yyyy") )
#set( $date = $ldt.format($dtf) )
#set( $year = $ldt.format($dtfYear) )
#set( $cleanVersion = "${version}" )
#set( $cleanVersion = $cleanVersion.replaceAll("-SNAPSHOT", "") )
/*
 *      File: ${appName}Application.java
 *    Author: Gerardo Zuniga <gerardo.zuniga@amk-technologies.com>
 *      Date: ${date}
 * Copyright: AMK Technologies, S.A. de C.V. ${year}
 */
package ${package};

//import javax.ws.rs.client.Client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.amk.sva.subs.controllers.ProductController;

import com.amk.sva.subs.it.adapters.exceptions.mappers.BuildingUrlLpExceptionMapper;
import com.amk.sva.subs.it.adapters.exceptions.mappers.ProductInvalidConfigurationExceptionMapper;
import com.amk.sva.subs.it.adapters.exceptions.mappers.ProductNotFoundExceptionMapper;
import com.amk.sva.subs.it.adapters.exceptions.mappers.ProductSearchingExceptionMapper;
import com.amk.sva.subs.it.adapters.exceptions.mappers.SubscriptionNotFoundExceptionMapper;
import com.amk.sva.subs.it.adapters.exceptions.mappers.SubscriptionStatusExceptionMapper;

import ${package}.filters.CORSResponseFilter;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

/**
 * ${projectName} application implemented with Dropwizard.
 *
 * @author Gerardo Zuniga &lt;gerardo.zuniga@amk-technologies.com&gt;
 * @version ${cleanVersion}
 * @since ${cleanVersion}
 */
public class ${appName}Application extends Application<${appName}Configuration> {

    /**
     * Class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(${appName}Application.class);

    /**
     * Starts the ${projectName} application.
     *
     * @param args Command line arguments.
     *
     * @throws Exception Exception in case of errors while starting the application.
     */
    public static void main(final String... args) throws Exception {
        LOGGER.info("Starting the ${projectName} application...");
        new ${appName}Application().run(args);
    }

    /**
     * Returns the name of the application.
     *
     * @return Name of the application.
     * @see io.dropwizard.Application${symbol_pound}getName()
     */
    @Override
    public String getName() {
        return "${projectName}";
    }

    /**
     * Initialize the application.
     *
     * @param bootstrap The bootstrap object used to initialize the application.
     * @see io.dropwizard.Application${symbol_pound}initialize(io.dropwizard.setup.Bootstrap)
     */
    @Override
    @SuppressFBWarnings("SIC_INNER_SHOULD_BE_STATIC_ANON")
    public void initialize(final Bootstrap<${appName}Configuration> bootstrap) {
        bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(
            bootstrap.getConfigurationSourceProvider(), new EnvironmentVariableSubstitutor(false)));
        bootstrap.addBundle(new AssetsBundle());
        bootstrap.addBundle(new SwaggerBundle<${appName}Configuration>() {
            /**
             * Retrieves the swagger configuration.
             *
             * @param config The Swagger configurtion object.
             * @see io.federecio.dropwizard.swagger.SwaggerBundle${symbol_pound}getSwaggerBundleConfiguration(io.dropwizard.Configuration)
             */
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(final ${appName}Configuration config) {
                return config.getSwaggerConfig();
            }
        });
    }

    /**
     * Runs the ${projectName} application.
     *
     * @param configuration The application configuration.
     * @param environment The application environment.
     *
     * @throws Exception Exception If an error occurs when runs the application.
     *
     * @see io.dropwizard.Application${symbol_pound}run(io.dropwizard.Configuration, io.dropwizard.setup.Environment)
     */
    @Override
    public void run(final ${appName}Configuration  configuration, final Environment environment) throws Exception {
        final CORSResponseFilter corsResponseFilter = new CORSResponseFilter();

        //final Client client = new JerseyClientBuilder(environment).using(configuration.getJerseyConfigSubsInfo())
        //    .build("Subscriptions Information API");
        //final ProductController productController = new ProductController(client, configuration.getUrlSubsInfo().concat("/product"));
        environment.jersey().register(corsResponseFilter);

        environment.jersey().register(new ProductNotFoundExceptionMapper());
        environment.jersey().register(new ProductSearchingExceptionMapper());
        environment.jersey().register(new ProductInvalidConfigurationExceptionMapper());
        environment.jersey().register(new BuildingUrlLpExceptionMapper());
        environment.jersey().register(new SubscriptionNotFoundExceptionMapper());
        environment.jersey().register(new SubscriptionStatusExceptionMapper());

    }
}
